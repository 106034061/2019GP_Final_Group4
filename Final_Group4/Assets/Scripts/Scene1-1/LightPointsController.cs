﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPointsController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] lights;
    public GameObject Player;
    public int Light_Transfer_Index = 0;
    private float distance =0;
    public const int Lights_Disappear_Distance = 10;
    public const int Lights_Intensity_max = 3;
    public const int Lights_Intensity_min = 0;

    void Start()
    {
        lights[0] = GameObject.Find("LightPointsController/Point Light");
        lights[1] = GameObject.Find("LightPointsController/Point Light (1)");
        lights[2] = GameObject.Find("LightPointsController/Point Light (2)");
        lights[3] = GameObject.Find("LightPointsController/Point Light (3)");
        lights[4] = GameObject.Find("LightPointsController/Point Light (4)");
        lights[5] = GameObject.Find("LightPointsController/Point Light (5)");

    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(lights[Light_Transfer_Index].transform.position.x - Player.transform.position.x) < Lights_Disappear_Distance &&
        Mathf.Abs(lights[Light_Transfer_Index].transform.position.z - Player.transform.position.z) < Lights_Disappear_Distance){
            Debug.Log("Light Transfer");
    	    lights[Light_Transfer_Index].GetComponent<Light>().intensity = Lights_Intensity_min;
            Light_Transfer_Index++;
    	    lights[Light_Transfer_Index].GetComponent<Light>().intensity = Lights_Intensity_max;
        }
    }
}
